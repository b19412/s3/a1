INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 1:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 2:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 3:00:00");
INSERT INTO users (email, password, datetime_created) VALUES ("maridadelacruz@gmail.com", "passwordD", "2021-01-01 4:00:00");
INSERT INTO users (email, password, datetime_created) VALUES("johndoe@gmail.com", "passwordE", "2021-01-01 5:00:00");

INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "First Code", "Hello World!", "2021-01-02 1:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Second Code", "Hello Earth!", "2021-01-02 2:00:00");
INSERT INTO posts (author_id, title, content, datetime_posted) VALUES (1, "Third Code", "Welcome to Mars!", "2021-01-02 3:00:00");
INSERT INTO posts (author_id, title, content,datetime_posted) VALUES (1, "First Code", "Bye bye solar system!", "2021-01-02 4:00:00");


SELECT * FROM posts WHERE author_id = 1;  

SELECT email, datetime_created FROM users;

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 1;
